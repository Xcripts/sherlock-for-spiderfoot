# Sherlock for Spiderfoot


## Descripción y requisitos

El script desarrollado en Python, implementa la herramienta "Sherlock" para el estudio de la utilización de nombres de usuarios en diferentes sitios web.

En su ejecución, solicita un nombre de usuario, y el resultado ofrecerá todos los sitios donde el nombre de usuario ha sido encontrado, junto con su URL.

## Requisitos

El módulo ha sido diseño y configurado para su ejecución en sistemas Linux. Las necesidades principales son:

	- Aplicación Sherlock instalada
	- Spiderfoot
	- Python 3.x

## Instalación y ejecución

Simplemente descargar el script de Python e incluir en la carpeta "modules" de Spiderfoot (spiderfoot/modules). Al ejecutar Spiderfoot, el módulo se cargará automáticamente.

Una vez cargado, es posible usarlo desde Spiderfoot seleccionandolo manualmente en la opción de módulos (sfp_sherlock), o bien mediante los grupos de ataque pasivo o footprint.

---------------------------------------------------------------------------------------------------------

## Description and requirements

The script developed in Python, implements the "Sherlock" tool for the study of the use of usernames in different web sites.

In its execution, it requests a username, and the result will offer all the sites where the username has been found, together with its URL.

## Requirements

The module has been designed and configured to run on Linux systems. The main requirements are:

	- Sherlock application installed
	- Spiderfoot
	- Python 3.x

## Installation and execution

Simply download the Python script and include it in the Spiderfoot "modules" folder (spiderfoot/modules). When Spiderfoot is run, the module will be loaded automatically.

Once loaded, it is possible to use it from Spiderfoot by selecting it manually in the modules option (sfp_sherlock), or by using the passive attack groups or footprint.
