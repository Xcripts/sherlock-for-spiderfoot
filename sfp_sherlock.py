# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------------
# Name:        sfp_sherlock
# Purpose:     Query Sherlock about username usage
#
# Author:      Daniel Consentini <dconsentini7@gmail.com>
#
# Created:     09/06/2023
# Copyright:   (c) Daniel Consentini 2023
# Licence:     GPL
# -------------------------------------------------------------------------------

import subprocess

from spiderfoot import SpiderFootEvent, SpiderFootPlugin

class sfp_sherlock(SpiderFootPlugin):

    meta = {
        'name': "Sherlock",
        'summary': "Use the Sherlock tool to check the use of a given username on social networks.",
        'flags': [""],
        'useCases': ["Footprint", "Passive"],
        'categories': ["Social Media"]
    }

    # Default options
    opts = {
    }

    # Option descriptions
    optdescs = {
    }

    results = None

    def setup(self, sfc, userOpts=dict()):
        self.sf = sfc
        self.results = self.tempStorage()

        for opt in list(userOpts.keys()):
            self.opts[opt] = userOpts[opt]

    # What events is this module interested in for input
    def watchedEvents(self):
        return ["USERNAME"]

    # What events this module produces
    # This is to support the end user in selecting modules based on events
    # produced.
    def producedEvents(self):
        return ["RAW_RIR_DATA"]

    # Handle events sent to this module
    def handleEvent(self, event):
        eventName = event.eventType
        srcModuleName = event.module
        eventData = event.data

        if eventData in self.results:
            return

        self.results[eventData] = True

        self.sf.debug(f"Received event, {eventName}, from {srcModuleName}")

        try:
            data = None

            self.sf.debug(f"We use the data: {eventData}")
            
            # Check Sherlock installation
            sherlock = str((subprocess.run('dpkg -s sherlock | grep Status', shell=True, capture_output=True, text=True)).stdout)

            if "install ok" in sherlock:

                # Sherlock process to search the username
                output = str((subprocess.run('sherlock '+ eventData, shell=True, capture_output=True, text=True)).stdout)

                allSites = output.split('\n')
                allSites.pop(0)

                # Remove empty lines
                while("" in allSites):
                    allSites.remove("")

            else:
                self.sf.error("Sherlock is not install ")
                return
        except Exception as e:
            self.sf.error("Unable to perform the action on " + eventData + ": " + str(e))
            return

        typ = "RAW_RIR_DATA"
        data = allSites

        # Send info to SpiderFoot
        for site in data:

            self.sf.debug(f"Site: {site}")
            
            evt = SpiderFootEvent(typ, site[4:], self.__name__, event)
            self.notifyListeners(evt)

# End of sfp_sherlock